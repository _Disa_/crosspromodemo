﻿using System.Collections.Generic;
using UnityEngine;

namespace CrosspromoDemo.Scripts.ObjectPool
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField] protected GameObject PoolGameObject;
        [SerializeField] protected int Count = 10;
        
        [Space(10)]
        public GameObject PoolderObjectsParent;
        protected List<GameObject> PoolderObjects;

        public List<GameObject> GetPoolderObjects()
        {
            return PoolderObjects;
        }

        /// <summary>
        /// Instantiate all objects
        /// </summary>
        protected virtual void Awake()
        {
            PoolderObjects = new List<GameObject>();
        
            for (var i = 0; i < Count; i++)
            {
                var go = Instantiate(PoolGameObject, PoolderObjectsParent.transform) as GameObject;
                go.SetActive(false);
                PoolderObjects.Add(go);
            }
        }
        
        /// <summary>
        /// Get object or instantiate if the pool is exhausted
        /// </summary>
        /// <returns>Expected object</returns>
        public virtual GameObject GetObject()
        {
            for (var i = 0; i < PoolderObjects.Count; i++)
            {
                if (!PoolderObjects[i].activeSelf)
                {
                    return PoolderObjects[i];
                }
            }
        
            var go = Instantiate(PoolGameObject, PoolderObjectsParent.transform) as GameObject;
            go.SetActive(false);
            PoolderObjects.Add(go);
        
            return go;
        }
        
        /// <summary>
        /// Release memory (destroy all poolder objects)
        /// </summary>
        public virtual void Release()
        {
            for (var index = 0; index < Count; index++)
            {
                var poolderObject = PoolderObjects[index];
                poolderObject.SetActive(false);
            }

            for (var index = Count; index < PoolderObjects.Count; index++)
            {
                Destroy(PoolderObjects[index]);
            }
            
            PoolderObjects.RemoveRange(Count, PoolderObjects.Count - Count);
        }
    }
}
