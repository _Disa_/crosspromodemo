﻿using CrosspromoDemo.Scripts.Bubble;
using UnityEngine;

namespace CrosspromoDemo.Scripts.ObjectPool
{
	public class BubbleObjectPool : ObjectPool
	{	
		protected override void Awake()
		{
			if (!PoolGameObject.GetComponent<PromoBubble>())
			{
				Debug.LogErrorFormat("'PoolGameObject' in 'BubbleObjectPool' should contains 'PromoBubble' component");
			}
			base.Awake();
		}
		
		/// <summary>
		/// Get object or instantiate if the pool is exhausted
		/// </summary>
		/// <returns>Expected object with PromoBubble component</returns>
		public virtual PromoBubble GetPromoBubbleObject()
		{
			return GetObject().GetComponent<PromoBubble>();
		}
		
		/// <summary>
		/// Release memory (destroy all poolder objects and their data release)
		/// </summary>
		public override void Release()
		{
			for (var index = 0; index < PoolderObjects.Count; index++)
			{
				var poolderObject = PoolderObjects[index];
				poolderObject.GetComponent<PromoBubble>().ReleaseMemory();
			}
			
			base.Release();
			
			Resources.UnloadUnusedAssets();
			PoolderObjectsParent.SetActive(false);
		}

		public void SetRect(Rect value)
		{
			if (PoolderObjects == null)
			{
				return;
			}
			
			for (var index = 0; index < PoolderObjects.Count; index++)
			{
				var floatBehavior = PoolderObjects[index].GetComponent<FloatBehavior>();
				if (floatBehavior)
				{
					floatBehavior.FloatingRect = value;
				}
			}
		}
	}
}
