﻿using System.Collections;
using UnityEngine;

namespace CrosspromoDemo.Scripts.Bubble
{
    public class PromoBubble : MonoBehaviour
    {
        public SpriteRenderer IconSpriteRenderer;
        public Sprite DefaultSprite;
        
        [HideInInspector]
        public bool IsAvailable;
        
        [Space(10)]
        [SerializeField] private float _delayBeforeOpenUrl = 1.0f;

        [Space(10)] 
        [SerializeField] private int _defaultTextureWidth  = 300;
        [SerializeField] private int _defaultTextureHeight = 300;
        
        private Vector3 _baseScale;
        
        private string _storeUrl;
        private Coroutine _coroutine;

        // Blow effect variables
        private Animator _animator;
        private AudioSource _audioSource;

        private const string BlowTriggerName = "Blow";
        private const string ResetTriggerName = "Reset";

        private int _blowTriggerHash;
        private int _resetTriggerHash;
        
        private void Awake()
        {
            _animator    = gameObject.GetComponent<Animator>();
            _audioSource = gameObject.GetComponent<AudioSource>();

            _blowTriggerHash  = Animator.StringToHash(BlowTriggerName);
            _resetTriggerHash = Animator.StringToHash(ResetTriggerName);

            _baseScale = IconSpriteRenderer.transform.localScale;
        }

        private void OnEnable()
        {
            _animator.SetTrigger(_resetTriggerHash);
            
            IconSpriteRenderer.transform.localScale = new Vector3(_baseScale.x * _defaultTextureWidth / IconSpriteRenderer.sprite.texture.width,
                                                                  _baseScale.y * _defaultTextureHeight / IconSpriteRenderer.sprite.texture.height,
                                                                  _baseScale.z);
        }

        /// <summary>
        /// Fetching data from recevied Firebase data
        /// </summary>
        /// <param name="textureUrl">Url to icon image</param>
        /// <param name="storeUrl">Scheme link to store app</param>
        /// <param name="script">MonoBehaviour whitch will execute coroutines</param>
        public void FetchData(string textureUrl, string storeUrl, MonoBehaviour script)
        {
            _storeUrl = storeUrl;
            
            if (_coroutine == null)
            {
                _coroutine = script.StartCoroutine(GetTextureCoroutine(textureUrl));
            }
        }
        
        private IEnumerator GetTextureCoroutine(string textureUrl)
        {
            IsAvailable = false;
            var www = new WWW(textureUrl);
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogErrorFormat("'GetTextureAsync' can't download texture from {0}", textureUrl);
                gameObject.SetActive(false);
            }
            else if (www.isDone)
            {
                IconSpriteRenderer.sprite = Sprite.Create(www.texture,
                                            new Rect(0, 0, www.texture.width, www.texture.height),
                                            new Vector2(0.5f, 0.5f));
                
                
                IsAvailable = true;
            }
            else
            {
                gameObject.SetActive(false);
            }

            _coroutine = null;
        }

        public void StartOpenUrlAfterDelay()
        {
            if (gameObject.activeSelf)
            {
                StartCoroutine(OpenUrlAfterDelay());
            }
        }

        private IEnumerator OpenUrlAfterDelay()
        {
            _audioSource.Play();
            _animator.SetTrigger(_blowTriggerHash);
            
            yield return new WaitForSecondsRealtime(_delayBeforeOpenUrl);
            Application.OpenURL(_storeUrl);

            yield return null;
            _animator.SetTrigger(_resetTriggerHash);
        }
        
        private void OnDisable()
        {
            StopAllCoroutines();
        }
        
        /// <summary>
        /// Destroy texture and sprite. Sets '_storeUrl' to empty string
        /// </summary>
        public void ReleaseMemory()
        {
            gameObject.SetActive(false);
         
            var oldSprite = IconSpriteRenderer.sprite;
            IconSpriteRenderer.sprite = DefaultSprite;

            if (oldSprite != null && oldSprite != DefaultSprite)
            {
                DestroyImmediate(oldSprite);
            }

            _storeUrl = "";
        }
    }
}
