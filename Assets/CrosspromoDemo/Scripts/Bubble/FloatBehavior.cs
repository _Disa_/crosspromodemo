﻿using UnityEngine;

namespace CrosspromoDemo.Scripts.Bubble
{
    // TODO
    // Think about less collision between bubbles
    public class FloatBehavior : MonoBehaviour
    {
        [SerializeField] private float _lerpFactorMin = 0.1f;
        [SerializeField] private float _lerpFactorMax = 0.4f;
        private float _lerpFactor;
        
        private Camera _bubbleCamera;
        private Vector3 _newTarget;
        
        [HideInInspector]
        public Rect FloatingRect;
        
        private void Awake()
        {
            _bubbleCamera = GameObject.FindGameObjectWithTag("BubbleCamera").GetComponent<Camera>();
            _lerpFactor = Random.Range(_lerpFactorMin, _lerpFactorMax);
        }
    
        private void OnEnable()
        {
            var x = -_bubbleCamera.orthographicSize * _bubbleCamera.aspect;
            var y = -_bubbleCamera.orthographicSize;
        
            transform.position = new Vector3(Random.Range(x, -x), y, transform.position.z);

            _newTarget.x = Random.Range(x * (FloatingRect.x - 0.5f) * -2.0f, -x * (FloatingRect.width - 0.5f) * 2.0f);
            _newTarget.y = Random.Range(y * (FloatingRect.y - 0.5f) * -2.0f, -y * (FloatingRect.height - 0.5f) * 2.0f);
        }

        private void Update()
        {
            transform.position = Vector3.Lerp(transform.position, _newTarget, Time.deltaTime * _lerpFactor);
            if (!(Vector3.Distance(transform.position, _newTarget) < 0.5f))
            {
                return;
            }
            
            _lerpFactor = Random.Range(_lerpFactorMin, _lerpFactorMax);
            
            var x = -_bubbleCamera.orthographicSize * _bubbleCamera.aspect;
            var y = -_bubbleCamera.orthographicSize;
            
            _newTarget.x = Random.Range(x * (FloatingRect.x - 0.5f) * -2.0f, -x * (FloatingRect.width - 0.5f) * 2.0f);
            _newTarget.y = Random.Range(y * (FloatingRect.y - 0.5f) * -2.0f, -y * (FloatingRect.height - 0.5f) * 2.0f);
        }
    }
}
