﻿using System.Collections.Generic;
using CrosspromoDemo.Scripts.ObjectPool;
using UnityEngine;

namespace CrosspromoDemo.Scripts.Managers
{
    public class PromoAppDataManager : MonoBehaviour
    {
        [SerializeField] private RemoteConfigManager _remoteConfigManager;
        [SerializeField] private BubbleObjectPool _bubbleObjectPoolRef;
        
        [Space(10)]
        [Tooltip("0, 0 - left buttom corner, 1, 1 - right top corner")]
        [SerializeField] private Rect _floatingRect;

        public Rect FloatingRect
        {
            get { return _floatingRect; }
            set { _bubbleObjectPoolRef.SetRect(value); }
        }

        private void Awake()
        {
            _remoteConfigManager.OnGetPromoAppData += GetPromoAppData;
        }

        private void Start()
        {
            FloatingRect = _floatingRect;
        }

        private void OnValidate()
        {
            FloatingRect = _floatingRect;
        }

        public void GetPromoAppData(IDictionary<string, List<string>> value)
        {
            foreach (var key in value.Keys)
            {
                var urls = value[key];

                var promoBubbleObject = _bubbleObjectPoolRef.GetPromoBubbleObject();
                promoBubbleObject.FetchData(urls[0], urls[1], this);
                promoBubbleObject.gameObject.SetActive(true);
            }
        }

        private void OnDestroy()
        {
            if (_remoteConfigManager.OnGetPromoAppData != null)
            {
                _remoteConfigManager.OnGetPromoAppData -= GetPromoAppData;
            }
        }
        
        public void ShowPromoBubbles()
        {
            _bubbleObjectPoolRef.PoolderObjectsParent.SetActive(true);
        }
        
        public void ReleasePromoBubbles()
        {
            _bubbleObjectPoolRef.Release();
        }
    }
}
