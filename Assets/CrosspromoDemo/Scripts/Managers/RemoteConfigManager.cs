﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Firebase.RemoteConfig;
using UnityEngine;

namespace CrosspromoDemo.Scripts.Managers
{
    [Serializable]
    public class RemoteConfigManager : MonoBehaviour
    {
        [SerializeField] private string _prefix = "promo_app_";
        
        private string _logMessage = "";
        private const int MaxLogMessageLenght = 16382;

        private Vector2 _scrollViewVector = Vector2.zero;
        
        private Firebase.DependencyStatus _dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
        private readonly IDictionary<string, List<string>> _promoAppData = new Dictionary<string, List<string>>();

        public IDictionary<string, List<string>> PromoAppData 
        {
            get { return _promoAppData; }
        }

        public Action<IDictionary<string, List<string>>> OnGetPromoAppData;
        
        /// <summary>
        /// Init Firebase on Start
        /// </summary>
        private void Start()
        {
            _dependencyStatus = Firebase.FirebaseApp.CheckDependencies();
    
            if (_dependencyStatus != Firebase.DependencyStatus.Available)
            {
                Firebase.FirebaseApp.FixDependenciesAsync().ContinueWith(task => 
                {
                    _dependencyStatus = Firebase.FirebaseApp.CheckDependencies();
                    if (_dependencyStatus == Firebase.DependencyStatus.Available)
                    {
                        InitializeAndFetchFirebase();
                    }
                    else
                    {
                        Debug.LogError("Could not resolve all Firebase dependencies: " + _dependencyStatus);
                    }
                });
            }
            else
            {
                InitializeAndFetchFirebase();
            }
        }
        
        private void InitializeAndFetchFirebase()
        {
            var defaults = new Dictionary<string, object>
            {
                {"promo_app_1", "http://i.imgur.com/jSKp6Gk.png;market://details?id=com.indigokids.mashacleanup"},
            };
            
            FirebaseRemoteConfig.SetDefaults(defaults);
            DebugLogForGui("RemoteConfig configured and ready!");

            FetchAndGetData();
        }
        
        /// <summary>
        /// Add log message to use it in gui debug view
        /// </summary>
        /// <param name="value"></param>
        public void DebugLogForGui(string value)
        {
            Debug.Log(value);
            _logMessage += value + "\n";

            while (_logMessage.Length > MaxLogMessageLenght)
            {
                _logMessage = _logMessage.Substring(_logMessage.IndexOf("\n", StringComparison.Ordinal) + 1);
            }

            _scrollViewVector.y = int.MaxValue;
        }
        
        /// <summary>
        /// Show all key by _prefix
        /// </summary>
        public void DisplayAllKeys()
        {
            DebugLogForGui(string.Format("GetKeysByPrefix(\"{0}\"):", _prefix));
            var keys = FirebaseRemoteConfig.GetKeysByPrefix(_prefix);
            
            foreach (var key in keys)
            {
                DebugLogForGui("    " + key);
            }
        }
        
        /// <summary>
        /// Get and save all fetching data to dictionary
        /// </summary>
        private void GetAllData()
        {
            _promoAppData.Clear();
            
            DebugLogForGui(string.Format("GetAllDataByPrefix(\"{0}\"):", _prefix));
            var keys = FirebaseRemoteConfig.GetKeysByPrefix(_prefix);
            
            foreach (var key in keys)
            {
                var result = ParsePromoDataValue(FirebaseRemoteConfig.GetValue(key).StringValue);
                if (result == null)
                {
                    continue;
                }
                
                DebugLogForGui(string.Format("Add data: (\"{0} : {1} , {2}\"):", key, result[0], result[1]));
                _promoAppData.Add(key, result);
            }

            if (OnGetPromoAppData != null && _promoAppData.Count > 0)
            {
                OnGetPromoAppData(_promoAppData);
            }
        }
        
        /// <summary>
        /// Parse data from firebase console
        /// </summary>
        /// <param name="value">Firebase string value</param>
        /// <returns>List with two elements - image url and scheme link to market</returns>
        private static List<string> ParsePromoDataValue(string value)
        {
            // TODO if required - add simple Regex to check valid data from link (url/scheme)
            Debug.LogFormat("ParsePromoDataValue: {0}", value);
            var result = value.Split(';');
            var urls = new List<string>();
            
            Debug.LogFormat("result Length: {0}", result.Length);
            
            if (result.Length != 2)
            {
                return null;
            }
            
            urls.Add(result[0]);  // example: http://i.imgur.com/jSKp6Gk.png
            Debug.LogFormat("urls[0]: {0}", urls[0]);
            
            urls.Add(result[1]);  // example: market://details?id=com.indigokids.mashacleanup
            Debug.LogFormat("urls[1]: {0}", urls[1]);
            
            return urls;
        }

        public void FetchAndGetData()
        {
            DebugLogForGui("Fetching data...");
         
            Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
            fetchTask.ContinueWith(FetchComplete);
        }

        private void FetchComplete(Task fetchTask)
        {
            if (fetchTask.IsCanceled)
            {
                DebugLogForGui("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                DebugLogForGui("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                DebugLogForGui("Fetch completed successfully!");
            }

            var info = FirebaseRemoteConfig.Info;
            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    FirebaseRemoteConfig.ActivateFetched();
                    DebugLogForGui(string.Format("Remote data loaded and ready (last fetch time {0}).", info.FetchTime));
                    GetAllData();
                    break;
                
                case LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case FetchFailureReason.Error:
                            DebugLogForGui("Fetch failed for unknown reason");
                            break;
                        
                        case FetchFailureReason.Throttled:
                            DebugLogForGui("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                        
                        case FetchFailureReason.Invalid:
                            DebugLogForGui("Fetch failure - Unknown error");
                            break;
                        
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    break;
                
                case LastFetchStatus.Pending:
                    DebugLogForGui("Latest Fetch call still pending.");
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
// *****************************************************
// *** Uncomment this block to show debug GUI in app ***
        private void OnGUI()
        {
            if (_dependencyStatus != Firebase.DependencyStatus.Available)
            {
                GUILayout.Label("One or more Firebase dependencies are not present.");
                GUILayout.Label("Current dependency status: " + _dependencyStatus);
                return;
            }

            var logArea = Screen.width < Screen.height ? 
                          new Rect(0.0f, 0.0f, Screen.width, Screen.height * 0.5f) : 
                          new Rect(Screen.width * 0.5f, 0.0f, Screen.width * 0.5f, Screen.height);

            GUILayout.BeginArea(logArea);
            _scrollViewVector = GUILayout.BeginScrollView(_scrollViewVector);
            GUILayout.Label(_logMessage);
            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }
// *****************************************************
        
    }
}
